# Instructions

## Description
This project is consisting of Craft 3 CMS, MySQL Database, Redis and PhpMyAdmin.

CMS, MySQL, Redis, PhpMyAdmin and Composer are encapsulated in in docker-containers.
CMS exposes port 80. PhpMyAdmin port 8080.

## Installation:
You have 2 ways to install the project. The way depends on how you want to setup. 
### New Setup (Clean install)
Install cms: by running:
```
composer create-project craftcms/craft
```
from the project root folder.

Install redis by running: 
```
composer require --prefer-dist yiisoft/yii2-redis -d ./app
``` 
from the project root folder.

1.  Copy ``.env`` file to the "app" folder.
2.  Fill it out and keep attention to the Quotes-notes.
3.  Copy files from `./congfig` to the `./app/config` folder.
    *it containers the configuration for the cms: redis connection and access rights for the user*
4.  Run `docker-compose up --build` and wait till completes.
5.  Go to `http://<HOSTNAME>/admin/install` or `http://<HOSTNAME>/index.php?p=admin` and complete the installation of CMS.

### Developing
1.  Ask for the `.env` file.
2.  Ask for the database dump.
3.  Place `.env` to the `./app` folder.
4.  Place `.env` to the `./` (root) folder.
4.  Run ``docker-compose up --build``
5.  Go to PhpMyAdmin Dashboard *(http://HOSTNAME:8080)*
6.  Enter user and password from from the `.env` file.
7.  Import the dump to the database
8.  Navigate to `http://<hostname>:80/admin`.  
*need notes on this installation option*

## Known Issues
*   ####CMS have limited set of menus in the control sidebar.
    __Solution:__ copy the contents of `config` folder to `./app/config`.
    
    __Explanation:__ it contains `general.php` file which configures user access right in different env.

*   ####PhpMyAdmin cant connect to the database. 
    __Solution:__ check the quotes in the env file.
    
    __Explanation:__ env file contains variables for the cms and mysql, phpmyadmin.
    Docker resolves the variables differently as CMS there for some variables are in quotes(CMS) and some not (docker).
*   ####CMS want install plugins.
    
    __Solution:__ Try couple of times again or install manually (see notes).
     
    
## Notes
*   You can backup you CMS files by exporting it from PhpMyAdmin.
*   You can add install CMS plugins by using composer in the app folder.