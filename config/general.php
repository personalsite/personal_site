<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 */
//return [
//    // Base site URL
//    'siteUrl' => null,
//    // Default Week Start Day (0 = Sunday, 1 = Monday...)
//    'defaultWeekStartDay' => 0,
//    // Enable CSRF Protection (recommended, will be enabled by default in Craft 3)
//    'enableCsrfProtection' => true,
//    // Whether "index.php" should be visible in URLs (true, false, "auto")
//    'omitScriptNameInUrls' => true,
//    // Control Panel trigger word
//    'cpTrigger' => 'admin',
//    // Dev Mode (see https://craftcms.com/support/dev-mode)
//    'devMode' => false,
//    // The secure key Craft will use for hashing and encrypting data
//    'securityKey' => getenv('SECURITY_KEY'),
//];

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => true,
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => false,
    ],

    // Staging environment settings
    'staging' => [
        // Prevent administrative changes from being made on staging
        'allowAdminChanges' => true,
    ],

    // Production environment settings
    'production' => [
        // Prevent administrative changes from being made on production
        'allowAdminChanges' => true,
    ],
];